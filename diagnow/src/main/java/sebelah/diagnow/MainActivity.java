package sebelah.diagnow;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

//functional java, kayanya ga kepake
//import fj.data.Array;
//import static fj.data.Array.array;
//import static fj.function.Integers.add;
//import static fj.pre.Show.arrayShow;
//import static fj.pre.Show.intShow;

import sebelah.diagnow.R;

public class MainActivity extends Activity {

    ImageView imgFavorite;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        open();
        imgFavorite = (ImageView)findViewById(R.id.imageView1);
        imgFavorite.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                open();
            }
        });
//        rgb2lab(120,120,120);
    }

    public void open(){
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bp = (Bitmap) data.getExtras().get("data");
//        imgFavorite.setImageBitmap(bp);
//        int picw=bp.getWidth(), pich=bp.getHeight();
//        int[] pix = new int[picw * pich];
//        bp.getPixels(pix, 0, picw, 0, 0, picw, pich);

        //int R, G, B,Y;
//
//
//        for (int y = 0; y < pich; y++){
//            for (int x = 0; x < picw; x++)
//            {
//                int index = y * picw + x;
//                int R = (pix[index] >> 16) & 0xff;     //bitwise shifting
//                int G = (pix[index] >> 8) & 0xff;
//                int B = pix[index] & 0xff;
//                //System.out.println(R);
                //Log.e("R",Integer.toString(R));
//                Log.e("ER",Integer.toString(R));

                //R,G.B - Red, Green, Blue
                //to restore the values after RGB modification, use
                //next statement
                //pix[index] = 0xff000000 | (R << 16) | (G << 8) | B;
//            }}
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public void rgb2lab(int R, int G, int B) {
        //http://www.brucelindbloom.com
        int []lab = new int[2];

        float r, g, b, X, Y, Z, fx, fy, fz, xr, yr, zr;
        float Ls, as, bs;
        float eps = 216.f/24389.f;
        float k = 24389.f/27.f;

        float Xr = 0.964221f;  // reference white D50
        float Yr = 1.0f;
        float Zr = 0.825211f;

        // RGB to XYZ
        r = R/255.f; //R 0..1
        g = G/255.f; //G 0..1
        b = B/255.f; //B 0..1

        // assuming sRGB (D65)
        if (r <= 0.04045)
            r = r/12;
        else
            r = (float) Math.pow((r+0.055)/1.055,2.4);

        if (g <= 0.04045)
            g = g/12;
        else
            g = (float) Math.pow((g+0.055)/1.055,2.4);

        if (b <= 0.04045)
            b = b/12;
        else
            b = (float) Math.pow((b+0.055)/1.055,2.4);


        X =  0.436052025f*r     + 0.385081593f*g + 0.143087414f *b;
        Y =  0.222491598f*r     + 0.71688606f *g + 0.060621486f *b;
        Z =  0.013929122f*r     + 0.097097002f*g + 0.71418547f  *b;

        // XYZ to Lab
        xr = X/Xr;
        yr = Y/Yr;
        zr = Z/Zr;

        if ( xr > eps )
            fx =  (float) Math.pow(xr, 1/3.);
        else
            fx = (float) ((k * xr + 16.) / 116.);

        if ( yr > eps )
            fy =  (float) Math.pow(yr, 1/3.);
        else
            fy = (float) ((k * yr + 16.) / 116.);

        if ( zr > eps )
            fz =  (float) Math.pow(zr, 1/3.);
        else
            fz = (float) ((k * zr + 16.) / 116);

        Ls = ( 116 * fy ) - 16;
        as = 500*(fx-fy);
        bs = 200*(fy-fz);

        lab[0] = (int) (2.55*Ls + .5);
        lab[1] = (int) (as + .5);
        lab[2] = (int) (bs + .5);
        Log.e("R",Integer.toString(lab[0]));
        Log.e("R",Integer.toString(lab[1]));
        Log.e("R",Integer.toString(lab[2]));

    }
}